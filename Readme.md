# Python Streaming and Translation client for apptek cloud
    
Assuming Python 3.6 or greater is installed.

# Setup python environment:
    python3 -m venv env
    source env/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt

# Generate pb files:
    ./compile.sh

# Running Applicatin 
    python streaming.py --license_key <Your license key> --audio_file <path to audio file> --source_lang_code <language code> --encoding <file encoding> --sample_rate <sample rate>

## With translation
    python streaming.py --license_key <Your license key> --audio_file <path to audio file> --source_lang_code <language code> --encoding <file encoding> --sample_rate <sample rate> --target_lang_code <target_lang_code>

# Options
    <Your license key> = license key provided by Apptek
    <path to audio file> = Audio file path
    <language code> = language code (Look at available language codes)
    <file encoding> = file encoding type (Look at supported file encodings)
    <sample rate> = sample rate (8000 or 16000)
    <target_lang_code> = machine translation langugae code (Look at available language pairs)


# Available language Codes
    To get available langugae codes 
    python get_available.py --license_key <Your license key>

This shall save currently available languages to "languages.json" in project root folder.

# Supported encodings 
    PCM_16bit_SI_MONO = 0;
    FLAC = 1;
    OGG_OPUS = 2;

# Text translation:
    python3 translation.py --license_key <Your license key> --text_file <path to input text file> --source_lang_code <source language code> --target_lang_code <target language code>

## Available language pairs:
    python3 translation.py --get_available
