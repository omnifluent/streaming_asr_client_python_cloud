import argparse
from transcribe_service import Client

def command_line():
    parser = argparse.ArgumentParser(
        prog='python get_available.py',
        description='Example streaming recognition client using AppTek ASR')
    
    parser.add_argument(
        '--license_key', required=True, type=str, help='Apptek license key')
    
    return parser.parse_args()


if __name__ == '__main__':
    config = command_line()
    # check parser options
    print('***** config *****')
    print(config)
    print('*****')
    try:
        client = Client(
            license_key=config.license_key,
            stream=None,
            source_lang_code=None,
            encoding=None,
            sample_rate=None,
            get_available=True,
            target_lang_code=None
            )
        
    except Exception as ex:
        print(ex)

