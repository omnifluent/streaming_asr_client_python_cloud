import apptek_pb2
import apptek_pb2_grpc
import grpc
import requests
import jwt
import sys

class Client:
    def __init__(self, license_key, source_lang_code=None, target_lang_code=None, domain=None,
                 extended_context=False):
        # acquire token using API key
            # make http get call to below url to get response with GATEWAY, TOKEN, 
        res = requests.get('https://license.apptek.com/license/v2/token/'+ license_key)
        if res.status_code == 200:
            print(res.text, file=sys.stderr)
            text = res.text.strip()
            decoded = jwt.decode(text, options={"verify_signature": False})
            print(decoded, file=sys.stderr)
            
            self._url = decoded['host'] + ':' + str(decoded['port'])
            if len(self._url):
                credentials = grpc.ssl_channel_credentials()
                channel = grpc.secure_channel(self._url, credentials)
                self._stub = apptek_pb2_grpc.GatewayStub(channel)
            self._token = text
            self._source_lang_code = source_lang_code
            self._target_lang_code = target_lang_code
            self._domain = domain
            self._extended_context = extended_context
            print('Connection ready', file=sys.stderr)
        else:
            print('**** Exception in getting token ****', file=sys.stderr)
            print(res, file=sys.stderr)

    # function to encode text message into grpc request.
    def _get_text_data(self, text, meta_options, num_hypotheses=0): # 0 - do not create n-best list, just use first-best

        return apptek_pb2.TranslateTextRequest(license_token=self._token,
                                               source_text = text,
                                               source_lang_code = self._source_lang_code,
                                               target_lang_code = self._target_lang_code,
                                               domain = self._domain,
                                               extended_context = self._extended_context,
                                               num_hypotheses = num_hypotheses,
                                               meta_options = meta_options)

    # send line to server for translation
    def translate(self, line, gender_code="", genre_code="",
                  length_code="", style_code="", num_hypotheses=0):
        meta_options = apptek_pb2.TranslateTextMetaOptions(gender_code=gender_code,
                                                           genre_code=genre_code,
                                                           length_code=length_code,
                                                           style_code=style_code)

        return self._stub.TranslateText(self._get_text_data(line, meta_options, num_hypotheses))
    
    # function to create available request token
    def _get_available_request(self):
        return apptek_pb2.AvailableRequest(license_token=self._token)
    
    # function to check if language pair is available
    def is_available(self, source_lang, target_lang, domain):
        for mt in self.get_available():
            if mt.source_language.lang_code == source_lang and \
                    mt.target_language.lang_code == target_lang and \
                    mt.domain == domain:
                return True
        return False

    # function to list available translation engines
    def get_available(self):
        credentials = grpc.ssl_channel_credentials()
        channel = grpc.secure_channel(self._url, credentials)
        stub = apptek_pb2_grpc.GatewayStub(channel)
        
        req = self._get_available_request()
        resp = stub.TranslateGetAvailable(req)
        return resp.list
