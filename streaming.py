import argparse
import sys
from transcribe_service import Client

def command_line():
    parser = argparse.ArgumentParser(
        prog='python streaming.py',
        description='Example streaming recognition client using AppTek ASR')
    parser.add_argument(
        '--audio_file', required=True, type=str, help='Audio file to stream')
    parser.add_argument(
        '--license_key', required=True, type=str, help='Apptek license key')
    parser.add_argument(
        '--source_lang_code',
        required=False,
        type=str,
        default='en-US')
    parser.add_argument(
        '--target_lang_code',
        required=False,
        type=str,
        default=None)
    parser.add_argument(
        '--encoding',
        required=False,
        type=int,
        choices=[0, 1, 2],
        default=0)
    parser.add_argument(
        '--sample_rate',
        required=False,
        type=int,
        choices=[16000, 8000],
        default=16000)
    
    return parser.parse_args()


if __name__ == '__main__':
    config = command_line()
    # check parser options
    # throw error if given options are not choosen
    print('***** config *****')
    print(config)
    print('*****')
    if config.sample_rate == 8000:
        if config.source_lang_code != 'en-US' and config.source_lang_code != 'es-US':
            print('********')
            print('For 8000Hz audio available Models are "en-US" or "es-US"')
            print('********')
            sys.exit(1)
    # aquire streaming client
    try:
        with open(config.audio_file, 'rb') as stream:
            # Client needs a .read()-supporting file-like object
            client = Client(
                license_key=config.license_key,
                stream=stream,
                source_lang_code=config.source_lang_code,
                # provide optional translation language code
                target_lang_code=config.target_lang_code,
                encoding=config.encoding,
                sample_rate=config.sample_rate,
                get_available=False)
            # generator function streaming_recognize() recognizes the audio
            # and returns the results on the fly
            results = client.streaming_recognize()
            # Resposne will be copied into results. Each result can be one of (Transcription/SegmentEnd/ProgressStatus)
            for result in results:
                print(result)
                print("*****")
    except Exception as ex:
        print(ex)

