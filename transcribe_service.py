import apptek_pb2
import apptek_pb2_grpc
import grpc
import requests
import time
import jwt
from google.protobuf.json_format import MessageToJson

class StreamObserver:
    def __init__(self):
        self.eof = False

    def set_eof(self):
        self.eof = True

    def is_eof(self):
        return self.eof

    def is_not_eof(self):
        return not self.eof


class Client:
    def __init__(self, license_key, stream, source_lang_code, target_lang_code, encoding, sample_rate, get_available):
        # acquire token using API key
            # make http get call to below url to get response with GATEWAY, TOKEN, 
        res = requests.get('https://license.apptek.com/license/v2/token/'+ license_key)
        if res.status_code == 200:
            print(res.text)
            text = res.text.strip()
            decoded = jwt.decode(text, options={"verify_signature": False})
            print(decoded)
            
            self._url = decoded['host'] + ':' + str(decoded['port'])
            self._token = text
            self._source_lang_code = source_lang_code,
            self._target_lang_code = target_lang_code,
            self._encoding = encoding,
            self._sample_rate = sample_rate,
            print('Connection ready')
            self._stream = stream
            if get_available == True:
                self.get_available()
        else:
            print('**** Exception in getting token ****')
            print(res)

    # function to prepare a configuration request. (First request should be a configuration request before sending audio packets).
    def _get_config(self):
        if (self._target_lang_code[0]):
            return apptek_pb2.RecognizeStreamRequest(

                configuration=apptek_pb2.RecognizeStreamConfig(
                    license_token=self._token,
                    audio_configuration=apptek_pb2.RecognizeAudioConfig(
                        encoding=self._encoding[0],
                        sample_rate_hz=self._sample_rate[0],
                        lang_code=self._source_lang_code[0],
                    ),
                    translate_configuration=apptek_pb2.RecognizeTranslateConfig(
                        translate_interval=0,
                        target_lang_code=self._target_lang_code[0]
                    ),
                ))
        else:
            return apptek_pb2.RecognizeStreamRequest(
                configuration=apptek_pb2.RecognizeStreamConfig(
                    license_token=self._token,
                    audio_configuration=apptek_pb2.RecognizeAudioConfig(
                        encoding=self._encoding[0],
                        sample_rate_hz=self._sample_rate[0],
                        lang_code=self._source_lang_code[0],
                    )))

    # function to encode audio packets from stream to grpc request.
    def _get_audio_data(self, data):
        return apptek_pb2.RecognizeStreamRequest(audio=data)

    # generator function (https://wiki.python.org/moin/Generators) to yield audio packets from a stream.
    def _streaming(self, stream_observer):
        # send initial configuration ticket
        yield self._get_config()
        # now stream audio
        more = True
        while more:
            data = self._stream.read(2048)
            more = len(data) > 0 and stream_observer.is_not_eof()
            # adding sleep just to simulate streaming(chunk_bytes / (sample_rate_Hz * 2))
            delay = 2048/(self._sample_rate[0] * 2)
            time.sleep(delay)
            if more:
                print('sending audio packets')
                yield self._get_audio_data(data=data)
            else:
                # at the end of file send a http get request to free up token.
                print('End of file')
                # end_res = requests.get('https://license.apptek.com/license/v1?request=free&token='+ self._token)
                print('Resource released')

    # function to generate a bidirectional grpc streaming client. 
    def streaming_recognize(self):
        if len(self._url):
            credentials = grpc.ssl_channel_credentials()
            channel = grpc.secure_channel(self._url, credentials)
            stub = apptek_pb2_grpc.GatewayStub(channel)
            
            # stream observer for responses.
            stream_observer = StreamObserver()
            
            for output in stub.RecognizeStream(self._streaming(stream_observer)):
                yield output
    
    # function to get available languages for streaming.
    def _get_available_request(self):
        print('returning token')
        return apptek_pb2.AvailableRequest(license_token=self._token)
    
    # function to create grpc client.
    def get_available(self):
        credentials = grpc.ssl_channel_credentials()
        channel = grpc.secure_channel(self._url, credentials)
        stub = apptek_pb2_grpc.GatewayStub(channel)
        
        resp = stub.RecognizeGetAvailable(self._get_available_request())
        # print(resp)
        # end_res = requests.get('https://license.apptek.com/license/v1?request=free&token='+ self._token)
        # print(end_res)
        with open('languages.json', mode='w') as localfile:
            localfile.write(MessageToJson(resp, including_default_value_fields=True, preserving_proto_field_name=True))

