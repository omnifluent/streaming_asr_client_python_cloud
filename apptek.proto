syntax = "proto3";

package gateway;
option go_package="gateway_grpc/gateway";

import "google/protobuf/empty.proto";
import "google/protobuf/wrappers.proto";

/*
* The AppTek gRPC gateway provides access to our streaming ASR service for over 30 languages and dialects.
* Our API provides apart from the realtime transcription of an audio stream also postprocessing (adding punctuation, capitalisation and translation of spoken forms of numbers, dates, spelled words, etc. into an easy to read and standard written form for most languages) and direct translation.
* Each request to the gateway requires a license token. See https://license.apptek.com/doc/index.html for details on how to acquire license tokens from the AppTek license server.
*/
service Gateway {
  /*
  * RecognizeGetAvailable returns all available languages for the RecognizeStream API.
  */
  rpc RecognizeGetAvailable(AvailableRequest) returns (RecognizeAvailableResponse) {}

  /*
  * RecognizeStream transcribes an incoming audio stream and returns the transcription in raw and postprocessed form and, if requested, translation into a target language.
  * The first and only the first message in the upstream must contain a RecognizeStreamConfig message, followed by messages containing audio data.
  * The audio data is automatically partitioned into segments, where each result message contains a segment id (starting with 1). Segment end events trigger a separate notification message.
  * During a segment returned results a subject to change. The last message in a segment is marked as "complete" and the next messages will be for the next segment.
  * The results for transcriptions, postprocessing and translations are independent of each other in the sense, that if e. g. the transcriptions already moved on to the next segments, trailing postprocessing and translation results can still be returned from the previous segment.
  * Not every transcription message triggers a corrensponding postprocessing and translation message, however it is guaranteed to always receive postprocessing and translation results for the "complete" segment transcription.
  * When diarization is enabled on every speaker change a RecognizeSpeakerChange is send containing the start time of the speaker and the speakers name.
  * Additionally on a complete segment a RecognizeDiarization message is send containing the postprocessed and optionally translated transcriptions for each speaker in that segment.
  * During the audio stream the API periodically returns progress status messages.
  */
  rpc RecognizeStream(stream RecognizeStreamRequest) returns (stream RecognizeStreamResponse) {}

  /*
  * TranslateGetAvailable returns all available languages for the TranslateText API.
  */
  rpc TranslateGetAvailable(AvailableRequest) returns (TranslateAvailableResponse) {}

  /*
  * TranslateText translates a source text into the provided target language.
  */
  rpc TranslateText(TranslateTextRequest) returns (TranslateTextResponse) {}

  /*
  * LineSegmentationGetAvailable returns all available languages for the LineSegmentation API.
  */
  rpc LineSegmentationGetAvailable(AvailableRequest) returns (LineSegmentationAvailableResponse) {}

  /*
  * LineSegmentation performs ILS (intelligent line segmentation) on a transcription and returns a SRT document.
  */
  rpc LineSegmentation(LineSegmentationRequest) returns (LineSegmentationResponse) {}

  /*
  * SpeakGetAvailable returns all available languages for the Speak API.
  */
  rpc SpeakGetAvailable(AvailableRequest) returns (SpeakAvailableResponse) {}

  /*
  * Speak generate speech from a given input text. The audio is returned in a stream where the first message
  * describes the content of the following audio data messages.
  */
  rpc Speak(SpeakRequest) returns (stream SpeakResponse) {}

  /*
  * SpeakAddReferenceAudio registers reference audio data which can be used to adapt the speaker voice.
  * The amount of reference audio data that can be registered is limited. The registered references automatically
  * expire after a while. Reference audio data can only be used with adaptable TTS engines.
  */
  rpc SpeakAddReferenceAudio(SpeakAddReferenceAudioRequest) returns (SpeakAddReferenceAudioResponse) {}

  /*
  * SpeakRemoveReferenceAudio removes a registered reference audio.
  */
  rpc SpeakRemoveReferenceAudio(SpeakRemoveReferenceAudioRequest) returns (google.protobuf.Empty) {}

  /*
  * Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
  *
  * Dub generates a dubbed version of a video by automatically translating and sythesising the spoken audio into the
  * target language.
  *
  */
  rpc Dub(stream DubbingStreamRequest) returns (DubbingStreamResponse) {}

  /*
  * Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!

  * DubStatus returns the current status of a submittet Dub request.
  *
  */
  rpc DubStatus(DubbingStatusRequest) returns (DubbingStatusResponse) {}

  /*
  * Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
  *
  * DubFile returns intermediate files from a Dub request.
  *
  */
  rpc DubFile(DubbingFileRequest) returns (stream DubbingFileResponse) {}
}

/*
* AvailableRequest contains the license token acquired be the AppTek license server
* to authenticate any request to list available engines.
*/
message AvailableRequest {
  string license_token = 1; // your license token
}

/*
* Direction indicates the display direction of a language
*/
enum Direction {
  Left2Right = 0; // left to right like in roman or germanic languages
  Right2Left = 1; // right to left like in arabic
}

/*
* Language description in detail
*/
message Language {
  string lang_code = 1; // language code
  string english_name = 2; // english name of the language
  string native_name = 3; // native namve of the language
  Direction direction = 4; // direction
}

/***************************************/
// Recognize messages
/***************************************/

/*
* RecognizeAvailable describes one available recognition engine
*/
message RecognizeAvailable {
  string domain = 1; // domain name
  uint32 sample_rate_hz = 2; // audio sample rate in Hz
  Language language = 3; // language details
  repeated Language mt_target_languages = 4; // list of available direct translations
  bool diarization = 5; // true if and only if diarization is available
}

/*
* RecognizeAvailableResponse contains the list of all available recognition engines.
* It is returned by the RecognizeGetAvailable API.
*/
message RecognizeAvailableResponse {
  repeated RecognizeAvailable list = 1; // list of all available recognition engines
}

/*
* RecognizeAudioEncoding lists all available audio encodings for the upstream
*/
enum RecognizeAudioEncoding {
  PCM_16bit_SI_MONO = 0; // PCM 16bit signed integer mono
}

/*
* RecognizeAudioConfig is the configuration message for the audio data in the RecognizeStream API.
* It must be send as the first and only the first message in the upstream.
*/
message RecognizeAudioConfig {
  RecognizeAudioEncoding encoding = 1; // encoding of the audio data
  uint32 sample_rate_hz = 2; // sample rate of the audio data in Hz
  string lang_code = 3; // language code of the audio
  string domain = 4; // domain name (leave empty if not sure)
  google.protobuf.UInt32Value mutable_suffix_length = 5; // maximum mutable suffix length (leave empty if not sure)
  bool diarization = 6; // enable diarization
  string custom_vocabulary_id = 7; // load custom vocabulary with this id
}

/*
* RecognizeTranslateInterval specifies the interval in which translation results are produced if direct translations of transcriptions is requested.
*/
enum RecognizeTranslateInterval {
  TRANSLATION_INTERVAL_SEGMENT_END = 0; // one translation only at segment end
  TRANSLATION_INTERVAL_CONTINUOUS = 1; // update translation multiple times during a segment.
}

/*
* RecognizeTranslateConfig is the optional translation configuration for the RecognizeStream API.
* Please make sure the requested target lang code is in the list of available mt target languages for your audio source language.
*/
message RecognizeTranslateConfig {
  RecognizeTranslateInterval translate_interval = 1; // interval in which translation results are produced
  string target_lang_code = 2; // lang code of the requested target language
}

/*
* RecognizeStreamConfig is the configuration message in the RecognizeStream API.
* It must be send as the first and only the first message in the upstream.
*/
message RecognizeStreamConfig {
  string license_token = 1; // your license token
  RecognizeAudioConfig audio_configuration = 2; // audio data configuration
  RecognizeTranslateConfig translate_configuration = 3; // optional translate configuration
}

/*
* RecognizeStreamRequest represents the upstream data in the RecognizeStream API.
* The first message must contain the configuration, all subsequent messages must contain audio data.
*/
message RecognizeStreamRequest {
  oneof oneof_request {
    RecognizeStreamConfig configuration = 1; // configuration message
    bytes audio = 2; // audio data
  }
}

/*
* RecognizeSegmentDetails contains the segment id (rolling number starting with 1) and a complete indicator. It is send with most result messages.
* If complete is set to true, the message will be the last message for the particular response type for the given segment id.
*/
message RecognizeSegmentDetails {
  uint32 id = 1; // segment id
  bool complete = 2; // true if and only if this is the last message for this response type with the given id, false otherwise
}

/*
* RecognizeWord describes all the details to a word in a RecognizeTranscription message
*/
message RecognizeWord {
  string word = 1; // recognized word
  uint64 start_time_ms = 2; // start time in milliseconds
  uint64 stop_time_ms = 3; // stop time in milliseconds
  float confidence = 4; // confidence score (between 0.0 and 1.0)
}

/*
* RecognizeTranscription is a partial recognition result.
*/
message RecognizeTranscription {
  RecognizeSegmentDetails segment = 1; // segment details
  string orth = 2; // the transcribed segment in one string
  repeated RecognizeWord words = 3; // list of all transcribed words with detailed information
}

/*
* RecognizeTranslation contains translation results.
*/
message RecognizeTranslation {
  RecognizeSegmentDetails segment = 1; // segment details
  string source = 2; // the source text for this translation
  string translation = 3; // the translated text
}

/*
* RecognizePostprocessing contains post-processed versions of recognized segments.
* For most languages we provide automatic postprocessing of raw recognition results in order to add punctuation, capitalisation and translation of spoken forms of numbers, dates, spelled words, etc. into an easy to read and standard written form.
* If postprocessing is not available for the source language, the postprocessed field matches the orth field.
* Not all RecognizeTranscription messages trigger a corrensponding RecognizePostprocessing message, but it is guaranteed to get the postprocessing result of the complete segment transcription.
*/
message RecognizePostprocessing {
  RecognizeSegmentDetails segment = 1; //segment details
  string orth = 2; // the source text
  string postprocessed = 3; // the postprocessed version of the source text
}

/*
* RecognizeProgressStatus messages are send from time to time to indicate the progress of the current stream.
* This is particular useful when segments in the audio do not contain speech, which might seem to make the recognizer unresponsive since there is nothing to return.
* Usually audio streams are limited in time. The remaining time value returns the remaining seconds of audio data the client is allowed to stream.
* All values are in seconds.
*/
message RecognizeProgressStatus {
  float audio_decoder_time_sec = 1; // audio decoder progress time in seconds
  float segmenter_progress_time_sec = 2; // segmenter progress time in seconds
  float recognizer_progress_time_sec = 3; // recognizer progress time in seconds
  float remaining_time_sec = 4; // remaining time in seconds
}

/*
* RecognizeSegmentEnd messages are send when the segmenter detects a segment end.
* These messages are helpful when the client intends to send a certain number of segments. Once the client receives the appropriate RecognizeSegmentEnd message, it can stop sending audio data.
*/
message RecognizeSegmentEnd {
  uint32 segment_id = 1; // segment id
  float segmenter_progress_time_sec = 2; // progress time in seconds since the last segment end in seconds
}

/*
* RecognizeSpeakerChange messages are send everytime a speaker change is detected.
* This requires to enable diarization in the RecognizeAudioConfig message.
*/
message RecognizeSpeakerChange {
  uint64 start_time_ms = 1; // start time of the speaker in milliseconds
  string speaker = 2; // speaker name
}

/*
* RecognizeDiarizationSpeakerSection are part of RecognizeDiarization messages.
* They contains the postprocessed (and optionally) translated transcription of a speaker within a complete segment.
*/
message RecognizeDiarizationSpeakerSection {
  string speaker = 1; // speaker name
  string orth = 2; // the transcribed text
  string postprocessed = 3; // the postprocessed text
  string translation = 4; // the translated test
  uint64 start_time_ms = 5; // start time of the speaker section in milliseconds
  uint64 stop_time_ms = 6; // stop time of the speaker section in milliseconds
}

/*
* RecognizeDiarization messages are send for each complete segment. They contain all speaker sections of the transcriptions and translations in this segment.
* This requires to enable diarization in the RecognizeAudioConfig message.
*/
message RecognizeDiarization {
  RecognizeSegmentDetails segment = 1; // segment details
  repeated RecognizeDiarizationSpeakerSection speaker_sections = 2; // all speaker sections
}

/*
* RecognizeStreamResponse represents the downstream data in the RecognizeStream API.
* It contains always one and only one response type.
*/
message RecognizeStreamResponse {
  oneof oneof_response {
    RecognizeTranscription transcription = 1; // a transcription response
    RecognizeSegmentEnd segment_end = 2; // a segment end response
    RecognizeProgressStatus progress_status = 3; // a progress status response
    RecognizePostprocessing postprocessing = 4; // a postprocessing response
    RecognizeTranslation translation = 5; // a translation response
    RecognizeSpeakerChange speaker_change = 6; // a speaker change response
    RecognizeDiarization diarization = 7; // a diarization response
  }
}

/***************************************/
// Translate messages
/***************************************/

/*
* TranslateTextFormat lists all available input formats for the TranslateText API.
*/
enum TranslateTextFormat {
  TRANSLATE_TEXT_FORMAT_PLAIN = 0; // plain text
  TRANSLATE_TEXT_FORMAT_SRT = 1; // SRT subtitle file (SubRip)
}

/*
* TranslateAvailable describes one available translation engine.
*/
message TranslateAvailable {
  string domain = 1; // domain name
  Language source_language = 2; // language details for the source text
  Language target_language = 3; // language details for the target text
  repeated TranslateTextFormat formats = 4; // available source text formats
  repeated string gender_codes = 5; // available gender codes
  repeated string genre_codes = 6; // available genre codes
  repeated string length_codes = 7; // available length codes
  repeated string style_codes = 8; // available style codes
  repeated string topic_codes = 9; // available topic codes
  bool extended_context = 10; // model supports translation with extended context
  bool compute_confidence = 11; //model supports computation of confidence scores
}

/*
* TranslateTextSrtOptions are optional parameters to tune the generation of
* translated SRT documents.
*/
message TranslateTextSrtOptions {
  google.protobuf.UInt32Value character_limit = 1; // maximum number of characters per line
  google.protobuf.UInt32Value max_lines = 2;   // maximum number of lines per frame
}

// TranslateTextMetaOptions are optional parameters to tune the translation.
message TranslateTextMetaOptions {
  // gender_code controls the grammar/morphological forms of the translation
  // (for example, female vs. male 1st person verb forms in Slavic languages),
  // in dependency on the gender of the speaker/author of the sentence
  string gender_code = 1;

  // genre_code provides information about the genre of the input text, the
  // translation is then adjusted to fit a particular genre
  // (for example, news, patents, etc.)
  string genre_code = 2;

  // length_code lets you control the length of the translation output,
  // in relation to the length of the input sentence
  string length_code = 3;

  // style_code controls the style/formality of the translation
  // (for example, using formal "you" instead of informal "you").
  string style_code = 4;

  // topic_code provides information about the topic of the input text, the
  // translation is then adjusted to fit a particular topic (for example, politics,
  // sports, culture within the genre of "news")
  string topic_code = 5;
}

/*
* TranslateAvailableResponse contains the list of all available translation engines.
* It is returned by the TranslateGetAvailable API.
*/
message TranslateAvailableResponse {
  repeated TranslateAvailable list = 1; // list of all available translation engines
}

/*
* TranslateTextRequest contains the request to translate a source text into the
* given target language.
*/
message TranslateTextRequest {
  string license_token = 1; // your license token
  string source_text = 2; // your plain source text
  string source_lang_code = 3; // lang code of the provided source text
  string target_lang_code = 4; // lang code of the requested target language
  string domain = 5; // domain name
  TranslateTextFormat format = 6; // source text format
  TranslateTextSrtOptions srt_options = 7; // optional parameters if source text is a SRT
  TranslateTextMetaOptions meta_options = 8; // optional parameters to tune the translation
  bool extended_context = 9; // use extended context
  bool compute_confidence = 10; // compute confidence scores, this should only be enabled when required, since it slows down the translation
  uint32 num_hypotheses = 11;
}

/*
* TranslateWordItem contains details for a single word.
*/
message TranslateTextWordItem {
  string word = 1; // the word
  float confidence = 2; // word confidence score
  bool is_attached_left = 3; // if word is attached to the previous word on the left
  int32 tag = 4; // for internal use
}

/*
* TranslateTextHypothesis contains the translation and details for one translation hypothesis.
*/
message TranslateTextHypothesis {
  string translation = 1; // translated text
  float confidence = 2; // hypothesis confidence score
  repeated TranslateTextWordItem word_items = 3; // list of all word items
}

/*
* TranslateTextSegment contains all hypotheses for one segment
*/
message TranslateTextSegment {
  repeated TranslateTextHypothesis hypotheses = 1; // list of all hypotheses
}

/*
* TranslateTextResponse contains the target text of a TranslateTextRequest
*/
message TranslateTextResponse {
  string target_text = 1; // target text of the best hypotheses in one string
  repeated TranslateTextSegment segments = 2; // detailed results for each translated segment
}

/***************************************/
// LineSegmentation messages
/***************************************/

// LineSegmentationAvailable describes one available language for ILS
message LineSegmentationAvailable {
  Language language = 1;
}

// LineSegmentationAvailableResponse contains the list of all available languages for ILS.
// It is returned by the LineSegmentationGetAvailable API.
message LineSegmentationAvailableResponse {
  repeated LineSegmentationAvailable list = 1;
}

// LineSegmentationSourceFormat lists all available source document formats for a
// line segmentation request.
enum LineSegmentationSourceFormat {
  LINE_SEGMENTATION_SOURCE_RASR_XML = 0; // RASR XML transcription file
}

// LineSegmentationOptions contains optional parameters in order to tune the output
// of a line segmentation request.
message LineSegmentationOptions {
  google.protobuf.UInt32Value character_limit = 1; // maximum number of characters per line
  google.protobuf.UInt32Value max_lines = 2;   // maximum number of lines per frame
  google.protobuf.FloatValue max_duration = 3; // maximum duration of a frame in seconds
  google.protobuf.FloatValue min_duration = 4; // minimum duration of a frame in seconds
  google.protobuf.FloatValue min_duration_between = 5; // minimum duration between two frames in seconds
  google.protobuf.FloatValue max_pause_within_sentence = 6; // maximum pause within a sentence in seconds

  // E.g. '>>' or '-'. Will be added in case of speaker changes when input format is XML and speaker labels are
  // available. Default is to not add a symbol.
  string new_speaker_symbol = 7;
}

// LineSegmentationRequest contains the request to perform a line segmentation from
// an input audio transcription into a SRT document.
message LineSegmentationRequest {
  string license_token = 1; // your license token
  LineSegmentationSourceFormat source_format = 2; // format of the source document
  string source= 3; // the source document
  string lang_code = 4; // language code of the source document
  LineSegmentationOptions options = 5; // optional parameters to tune the output
}

// LineSegmentationResponse contains the target document of a line segmentation request.
message LineSegmentationResponse {
  string target = 1; // target document (SRT)
}

/***************************************/
// Speak messages
/***************************************/

/*
* SpeakAvailable describes one available TTS engine.
*/
message SpeakAvailable {
  string domain = 1; // domain name
  Language language = 2; // language details
  string speaker = 3; // TTS speaker name
  bool adaptable = 4; // adaptable TTS allow to adapt the speaker voice with reference audio data
  string gender = 5; // gender of the TTS speaker (might be empty, e. g. for adaptable speakers)
}

/*
* SpeakReferenceAudioEncoding lists all available audio encodings for the upstream
*/
enum SpeakReferenceAudioEncoding {
  TTS_REF_PCM_16000_16bit_SI_MONO = 0; // PCM 16000 Hz 16bit signed integer mono
}

/*
* SpeakAddReferenceAudioRequest contains your reference audio to be registered.
* It can be used later to produce speech with an adapted voice.
* The reference audio must not be longer than 30sec.
*/
message SpeakAddReferenceAudioRequest {
  string license_token = 1; // your license token
  SpeakReferenceAudioEncoding encoding = 2; // encoding of the reference audio data
  bytes audio = 3; // your reference audio data
}

/*
* SpeakAddReferenceAudioResponse returns the unique id of the registered reference
* audio data. Use this id in your speak request to adapt the speakers voice.
* The audio is automatically removed from the server at the expiration time or
* manually with using the SpeakRemoveReferenceAudio API.
*/
message SpeakAddReferenceAudioResponse {
  string id = 1; // id of the reference audio data
  int64 exp = 2; // expiration time in second since epoch
}

/*
* SpeakRemoveReferenceAudio removed the reference audio from the server.
*/
message SpeakRemoveReferenceAudioRequest {
  string license_token = 1; // your license token
  string id = 2; // id of the reference audio data
}

/*
* SpeakAvailableResponse contains the list of all available TTS engines.
* It is returned by the SpeakGetAvailable API.
*/
message SpeakAvailableResponse {
  repeated SpeakAvailable list = 1; // list of all available TTS engines
}

// SpeakAudioEncoding are the available TTS audio data formats.
enum SpeakAudioEncoding {
  TTS_PCM_22050_16bit_SI_MONO = 0; // PCM 22050 Hz 16bit signed integer mono
  TTS_PCM_16000_16bit_SI_MONO = 1; // PCM 16000 Hz 16bit signed integer mono
  TTS_PCM_24000_16bit_SI_MONO = 2; // PCM 24000 Hz 16bit signed integer mono
  TTS_PCM_48000_16bit_SI_MONO = 3; // PCM 48000 Hz 16bit signed integer mono
}

/*
* SpeakRequest contains the request to convert an input text into audio data.
*/
message SpeakRequest {
  string license_token = 1; // your license token
  SpeakAudioEncoding encoding = 2; // requested encoding of the output audio stream
  string text = 3; // input text
  string lang_code = 4; // lang code of the input text
  string speaker = 5; // TTS speaker name
  string domain = 6; // domain name
  // tempo specifies the playback speed of the generated audio file. If set to 0.0
  // it defaults internally to normal speed (1.0), values between 0.5 and 1.5 are
  // applicable
  float tempo = 7;
  // id of a registered reference audio to adapt the speakers voice
  string reference_audio_id = 8;
}

// SpeakMeta returns the details of the TTS audio data.
message SpeakMeta {
  SpeakAudioEncoding encoding = 1; // encoding of the TTS audio data
}

// SpeakResponse contains the generated audio data from a speak request
message SpeakResponse {
  oneof oneof_response {
    // the first and only the first message is a meta message describing the content
    // of the following audio messages
    SpeakMeta meta = 1;
    // the audio containing the generated speech
    bytes audio = 2;
  }
}

/***************************************/
// Dubbing messages
/***************************************/

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingStreamConfig {
  string license_token = 1; // your license token
  string domain = 2;
  string source_lang_code = 3;
  string target_lang_code = 4;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingStreamRequest {
  oneof oneof_request {
    DubbingStreamConfig configuration = 1;
    bytes video = 2;
  }
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingStreamResponse {
  string request_id = 1;
  string sha256 = 2;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingStatusRequest {
  string license_token = 1;
  string request_id = 2;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
enum DubbingState {
  DUBBING_STATE_PENDING = 0;
  DUBBING_STATE_PROCESSING = 1;
  DUBBING_STATE_DONE = 2;
  DUBBING_STATE_ERROR = 3;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingStatusResponse {
  DubbingState state = 1;
  string message = 2;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFileListRequest {
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFileDownloadRequest {
  string filename = 1;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFileRequest {
  string license_token = 1;
  string request_id = 2;
  oneof oneof_request {
    DubbingFileListRequest list = 3;
    DubbingFileDownloadRequest download = 4;
  }
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFile {
  string filename = 1;
  string created_by = 2;
  int64 bytes = 3;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFileListResponse {
    repeated DubbingFile files = 1;
}

// Preview: The dubbing API is work in progress and might undergo compatibility breaking change in the future!
message DubbingFileResponse {
  oneof oneof_response {
    bytes chunk = 1;
    DubbingFileListResponse list = 2;
  }
}
