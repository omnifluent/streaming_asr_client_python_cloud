import argparse
import sys
from translation_service import Client

def command_line():
    parser = argparse.ArgumentParser(
        prog='python translation.py',
        description='Example translation client using AppTek MT')
    parser.add_argument('--text_file', help='Text file to translate')
    parser.add_argument('--license_key', required=True, help='Apptek license key')
    parser.add_argument('--source_lang_code', default='en-US')
    parser.add_argument('--target_lang_code')
    parser.add_argument('--batch_translation', default=False, action='store_true')
    parser.add_argument('--domain', default='general')
    parser.add_argument('--get_available', default=False, action='store_true',
            help='Requests list of available services')
    return parser.parse_args()


if __name__ == '__main__':
    config = command_line()
    # check parser options
    # throw error if given options are not choosen
    print('***** config *****')
    print(config)
    print('*****')
    if config.get_available:
        client = Client(license_key=config.license_key)
        print('Available translation engines:')
        for mt in client.get_available():
            print(f"{mt.source_language.lang_code}->{mt.target_language.lang_code} {mt.domain} "
                  f"({mt.source_language.english_name} -> {mt.target_language.english_name})")
    else:
        assert config.target_lang_code, "--target_lang_code is required"
        assert config.text_file, "--text_file is required"
        # acquire streaming client
        client = Client(
            license_key=config.license_key,
            source_lang_code=config.source_lang_code,
            target_lang_code=config.target_lang_code,
            domain=config.domain)
        with open(config.text_file, 'rt', encoding='utf-8') as input_text:
            if config.batch_translation:
                source_text = input_text.read()
                result = client.translate(source_text)
                print('***** source_text:\n' + source_text)
                result = client.translate(source_text)
                print('***** target_text:\n' + result.target_text)
                print("*****")
            else:
                print("*****")
                for line in input_text:
                    print('source_text: ', line.rstrip())
                    result = client.translate(line)
                    print('target_text: ', result.target_text.rstrip())
                    print("*****")
